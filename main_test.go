package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestHelloHandler(t *testing.T) {
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	HelloHandler(w, req)

	expected := "hello, world!"
	actual := w.Body.String()
	if !strings.HasPrefix(actual,expected) {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}

func TestPingHandler(t *testing.T) {
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/ping", nil)
	PingHandler(w, req)

	expected := "Pong -"
	actual := w.Body.String()
	if !strings.HasPrefix(actual,expected) {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}

### Steps to deploy a golang-app to azure app service (web app)
This steps belongs to the tutorial from https://github.com/Durgaprasad-Budhwani/docker-azure-web-app-golang 
*****

#### golang application
Simple helloworld application which serves httphandler on given port

* HelloHandler return hello world message `/`
* PingHandler Return message "Pong - " with current time in Month Day Time format `/ping`

#### build golang application
`./build.sh build`

#### run golang application
* Run local go: `go run main.go`
* Run docker: `./build.sh run`
* Run docker-compose: `docker-compose up && docker-compose rm -f`

Open a browser and hit URL `http://localhost:8000/`

The code snippet above exposes 2 APIs:

* `/` will return *“hello world!” message*
* `/ping` will return *Pong message*.

#### tag and push golang application to docker hub
```
 docker login https://index.docker.io/v1/
./build.sh tagpush
```


#### Azure Auto Deploy Newly Published Docker image
Check https://github.com/Durgaprasad-Budhwani/docker-azure-web-app-golang#azure-auto-deploy-newly-published-docker-image




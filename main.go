package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// HelloHandler return hello world message`
func HelloHandler(w http.ResponseWriter, _ *http.Request) {
	_, err := io.WriteString(w, "hello, world!\nThis is a Golang test application!\nTry /ping! \n")
	if err != nil {
		fmt.Println("Error HelloHandler: ", err)
	}
}

// PingHandler Return message "Pong - " with current time in Month Day Time format
func PingHandler(w http.ResponseWriter, _ *http.Request) {
	t := time.Now()
	_, err := io.WriteString(w, fmt.Sprint("Pong - ", t.Format("Mon Jan _2 15:04:05 2020")))
	if err != nil {
		fmt.Println("Error PingHandler: ", err)
	}
}

func main() {
	fmt.Println("Server started .....")

	// HandleFunc registers the handler function for the given pattern
	// in the DefaultServeMux.
	http.HandleFunc("/", HelloHandler)
	http.HandleFunc("/ping", PingHandler)

	//// Get port number from environment
	port := os.Getenv("PORT")
	if port == "" {
		port = "3006"
	}

	fmt.Printf("Listen serve for http://localhost:%s/ping\n", port)

	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		// in case of error, log.Fatal will exit application
		log.Fatal(err)
	}

}
